import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;
 
 class TextEditor
 {
      static void updateStatus(int linenumber, int columnnumber,JTextField status)
      {
         status.setText("Line: " + linenumber + " Column: " + columnnumber);
      }
      public static void main(String[] args)
     {
        int i = 10;
 
        final JTextField status = new JTextField();
        final JTextArea textArea = new JTextArea();
        final JFrame frame = new JFrame("Untitled-Text Editor");
        JMenuBar framemenu = new JMenuBar();
     //final String filename = null ;
 
        JButton abt = new JButton("About");
        JButton opn = new JButton("Open");
        opn.setMnemonic('O');
        JButton nw = new JButton("New");
        nw.setMnemonic('N');
        JButton sve = new JButton("Save");
        sve.setMnemonic('S');
        JButton fnd = new JButton("Find");
        fnd.setMnemonic('F');
        JButton rplce = new JButton("Replace All");
        rplce.setMnemonic('R');
 
        framemenu.add(nw);
        framemenu.add(opn);
        framemenu.add(sve);
        framemenu.add(fnd);
        framemenu.add(rplce);
        framemenu.add(abt);
 
        abt.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                JOptionPane.showMessageDialog(null, "Product Name:Text Editor(A Simple Text Editor using Java)\nVersion:1.0\nAuthor:Adhithyan Vijayakumar\nRoll no:2011103592", "About-Text Editor", 1);
            }
        });
        nw.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                textArea.setText("");
                String str = "Untitled";
                frame.setTitle(str + "-Text Editor");
            }
        });
        opn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                textArea.setText("");
                JFileChooser file = new JFileChooser();
                //file .setFileFilter(".txt");
                int i = file.showOpenDialog(frame);
                if (i == 0)
                {
                    try
                    {
                        BufferedReader br = new BufferedReader(new FileReader(file.getSelectedFile()));
                        String str2 = file.getSelectedFile().getName();
                        String str1 = br.readLine();
                        frame.setTitle(str2);
                        while (str1 != null)
                        {
                            textArea.append(str1 + "\n");
                            str1 = br.readLine();
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
                else
                    JOptionPane.showMessageDialog(null, "File not selected");
            }
        });
        sve.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                JFileChooser file = new JFileChooser();
                int i;
                File fy;
                String str1;
                i = file.showSaveDialog(frame);
                fy = file.getSelectedFile();
                str1 = fy.getName();
                frame.setTitle(str1);
                if (i == 0)
                {
                    try {
                            String str2 = textArea.getText();
                            BufferedWriter br = new BufferedWriter(new FileWriter(file.getSelectedFile()));
                            br.write(str2);
                            br.close();
                        }
                    catch (Exception exc)
                    {
                    }
                }
 
                else
                {
                    JOptionPane.showMessageDialog(null, "Operation cancelled by User");
                }
 
            }
        });
 
        fnd.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent paramAnonymousActionEvent)
            {
                try
                {
                    Highlighter hilite = textArea.getHighlighter();
                    String str1 = textArea.getText();
                    String str2 = JOptionPane.showInputDialog(null, "Enter text to find:");
                    int i = 0; int count = 0;
 
                    hilite.removeAllHighlights();
 
                    while ((i = str1.indexOf(str2, i)) >= 0)
                    {
                        hilite.addHighlight(i, i + str2.length(), DefaultHighlighter.DefaultPainter);
                        i += str2.length();
                        count++;
                    }
                    JOptionPane.showMessageDialog(null, count + " matches found");
                }
                catch (BadLocationException localBadLocationException)
                {
                }
            }
        });
        rplce.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                Highlighter hilite = textArea.getHighlighter();
                String str1 = textArea.getText();
                String str2 = JOptionPane.showInputDialog(null, "Enter text to find:");
                String str3 = JOptionPane.showInputDialog(null, "Enter text to replace:");
                int i = 0;
                int count = 0;
                hilite.removeAllHighlights();
                if (textArea.getText().contains(str2))
                textArea.setText(textArea.getText().replaceAll(str2, str3));
                while ((i = str1.indexOf(str2, i)) >= 0)
                {
                    i += str2.length();
                    count++;
                }
                JOptionPane.showMessageDialog(null, count + " replaces made");
            }
        });
 
        textArea.addCaretListener(new CaretListener()
        {
            public void caretUpdate(CaretEvent e)
            {
                JTextArea editArea = (JTextArea)e.getSource();
                int linenum = 1;
                int columnnum = 1;
 
                try {
                    int caretpos = editArea.getCaretPosition();
                    linenum = editArea.getLineOfOffset(caretpos);
                    columnnum = caretpos - editArea.getLineStartOffset(linenum);
                    linenum += 1;
                }
                catch(Exception ex) { }
 
                updateStatus(linenum, columnnum,status);
            }
        });
 
        WindowListener exitListener = new WindowAdapter()
        {
            public void windowClosing(WindowEvent e) {
                if(frame.getTitle().equals("Untitled-Text Editor")&& !textArea.getText().equals(""))
                {
                    int confirm = JOptionPane.showOptionDialog(null, "Exit without saving?", "Exit Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
                    if (confirm == 0) {
                            System.exit(0);
                                    }
                    else
                    {
                        JFileChooser file = new JFileChooser();
                        int i = file.showSaveDialog(frame);
                        File fy = file.getSelectedFile();
 
                        if (i == 0)
                        {
                            //File fy = file.getSelectedFile();
                            String str1 = fy.getName();
                            frame.setTitle(str1 + " -Text Editor");
                            try
                            {
                                String str2 = textArea.getText();
                                BufferedWriter br = new BufferedWriter(new FileWriter(file.getSelectedFile()));
                                br.write(str2);
                                br.close();
                                //System.exit(0);
                            }
                            catch (Exception ex)
                            {
                                //ex.printStackTrace();
                            }
                        }
                        System.exit(0);
 
                    }
                }
            }
        };
        frame.addWindowListener(exitListener);
     if(args.length>0)
     {
        try
        {   BufferedReader br = new BufferedReader(new FileReader(args[0]));
            String str = br.readLine();
            frame.setTitle(args[0]+"-Text Editor");
            while(str!=null)
            {
                textArea.append(str+"\n");
                str = br.readLine();
            }
        }
        catch(Exception e){}
    }
 
        frame.add(status, BorderLayout.SOUTH);
        updateStatus(1,1,status);
 
     textArea.setFont(new Font("Arial", 0, 16));
     //textArea.setForeground(Color.BLUE);
     textArea.setLineWrap(true);
     frame.setJMenuBar(framemenu);
     JScrollPane scroll = new JScrollPane(textArea);
     //textArea.setBackground(Color.GREEN);
     scroll.setVerticalScrollBarPolicy(22);
     frame.add(scroll);
     frame.setSize(500, 500);
     frame.setVisible(true);
     //frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
 }
}